<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'thumbnail_url',
    ];

    protected $with = [ 'comics' ];


    public function comics() {
        return $this->belongsToMany(Comic::class, AuthorComic::class);
    }
}
