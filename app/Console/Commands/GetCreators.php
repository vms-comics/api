<?php

namespace App\Console\Commands;

use App\Http\Helpers\MarvelApiWrapper;
use Illuminate\Console\Command;

class GetCreators extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'creators:get {limit=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Marvel creators data';


    /**
     * Number of records to get
     *
     * @var integer
     */
    protected $limit;


    /**
     * MarvelApiWrapper object
     *
     * @var MarvelApiWrapper
     */
    protected $marvel;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->marvel = new MarvelApiWrapper;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        $this->limit = $this->argument('limit');
        
        if( $this->limit > 10 ) {
            $this->error("Limit must not exceed 10 records.");
        }

        $response = $this->marvel->fetch('creators', $this->limit);
        $this->info($response);
        return (int) ($response['status'] !== 'Ok');
    }
}
