<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Intersection table for Comics belonging to Author
 * 
 */

class AuthorComic extends Model
{
    use HasFactory, SoftDeletes;
    
    public $timestamps = false;

    protected $fillable = [
        'author_id',
        'comic_id',
    ];

    /**
     * Author owning the comics
     *
     * @return BelongsTo
     */
    public function author() : BelongsTo  {
        return $this->belongsTo(Author::class);
    } 

    /**
     * Comic belonging to Author
     *
     * @return BelongsTo
     */
    public function comic() : BelongsTo  {
        return $this->belongsTo(Comic::class);
    }
}
