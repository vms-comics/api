<?php

namespace App\Http\Helpers;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

/**
 * Wrapper class for Marvel API
 * 
 * @author "Joe Estrella" <jhourlad01@gmail.com>
 * @link https://developer.marvel.com
 * 
 */

class MarvelApiWrapper
{
    /**
     * Marvel API base URL
     *
     * @var string
     */

    protected $url;

    /**
     * Marvel API public key
     *
     * @var string
     */

    protected $apiKey;

    /**
     * Marvel API private key
     *
     * @var string
     */

    protected $apiSecret;


    /**
     * constructor
     *
     * @return void
     */

    function __construct()
    {
        $this->url = env('MARVEL_API_URL');
        $this->apiKey = env('MARVEL_API_KEY');
        $this->apiSecret = env('MARVEL_PRIVATE_KEY');
    }


    /**
     * Send a valid GET request to Marvel API
     * 
     * @param string $endpoint
     * @param int $limit
     * @return Response
     */

    public function fetch(string $endpoint, int $limit=10): Response
    {
        return Http::get($this->_url($endpoint, $limit));
    }


    /**
     * Create a Marvel API-compliant querystring
     * 
     * @param string $endpoint
     * @return string
     *  
     */

    private function _url(string $endpoint, int $limit = 10): string
    {
        $ts = time();

        return $this->url . '/' . $endpoint . '?' . http_build_query([
            'limit' => $limit,
            'ts' => $ts,
            'apikey' => $this->apiKey,
            'hash' => $this->_hash($ts),
        ]);
    }


    /**
     * Create a Marvel API-compliant hash based on app credentials
     * 
     * @param int $ts
     * @return string
     *  
     */

    private function _hash(int $ts): string
    {
        return md5("{$ts}{$this->apiSecret}{$this->apiKey}");
    }
}
