# VMSComicsApi

API web service for VMS Comics

## Installation

Install and serve as you would any Laravel project.

```bash
composer install
php artisan migrate
php artisan serve
```
## Console commands

Get creators

```bash
php artisan creators:get
```

Import authors and comics

```bash
php artisan comics:import
```
