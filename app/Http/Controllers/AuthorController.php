<?php

namespace App\Http\Controllers;

use App\Models\Author;

class AuthorController extends Controller
{
    /**
     * Constructor 
     */

    public function __construct()
    {
        $this->model = new Author();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function comics(Author $author)
    {
        return response($author);
    }
}
