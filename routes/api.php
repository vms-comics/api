<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\ComicController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () use ($router) {
    return 'VMS Comics API';
});

Route::middleware('foobar')->group(function() {
    Route::resource('authors', AuthorController::class);
    Route::group(['prefix' => 'author/{author}'], function() {
        Route::get('/', [AuthorController::class, 'show']);
        Route::get('comics', [AuthorController::class, 'comics']);
    });    

    Route::resource('comics', ComicController::class);
});
