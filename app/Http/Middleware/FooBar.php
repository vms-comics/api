<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class FooBar
{
    protected $token = "baz";

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $foobar = $request->header('foo_bar');

        if( $foobar == $this->token ) {
            return $next($request);
        }

        return response()->json(['error' => 'Forbidden'], 403);        
    }
}
