<?php

namespace App\Http\Controllers;

use App\Models\Comic;

class ComicController extends Controller
{

    /**
     * Constructor 
     */

    public function __construct()
    {
        $this->model = new Comic();
    }
}
