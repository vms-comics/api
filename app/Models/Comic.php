<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comic extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'title',
        'series_name',
        'description',
        'page_count',
        'thumbnail_url',
    ];

    public function authors() {
        return $this->belongsToMany(Author::class, AuthorComic::class);
    }
}
