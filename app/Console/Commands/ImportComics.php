<?php

namespace App\Console\Commands;

use App\Http\Helpers\MarvelApiWrapper;
use App\Models\Author;
use App\Models\AuthorComic;
use App\Models\Comic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportComics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comics:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Marvel creators comics data';


    /**
     * MarvelApiWrapper object
     *
     * @var MarvelApiWrapper
     */
    protected $marvel;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->marvel = new MarvelApiWrapper;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        $authors = $this->marvel->fetch('creators');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Author::truncate();
        AuthorComic::truncate();
        Comic::truncate();

        if( $authors['status'] == 'Ok') {
            collect($authors->json()['data']['results'])->each(function($item) {
                $author = Author::create([
                    'first_name' => $item['firstName'],
                    'last_name' => $item['lastName'],
                    'thumbnail_url' => "{$item['thumbnail']['path']}/standard_fantastic.{$item['thumbnail']['extension']}",
                ]);

                $comics = $this->marvel->fetch("creators/{$item['id']}/comics");

                collect($comics->json()['data']['results'])->each(function($item) use($author) {
                    $comic = Comic::firstOrCreate(
                        [ 'title' => $item['title'] ],
                        [
                            'title' => $item['title'],
                            'series_name' => $item['series']['name'],
                            'description' => $item['description'],
                            'page_count' => $item['pageCount'],
                            'thumbnail_url' => "{$item['thumbnail']['path']}/standard_fantastic.{$item['thumbnail']['extension']}",
                        ]
                    );

                    $author->comics()->attach($comic->id);
                });    
            });

            DB::statement('SET FOREIGN_KEY_CHECKS=1');
            return 0;
        }

        return 1;
    }
}
